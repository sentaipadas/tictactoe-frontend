## Setup 
1. Clone the project:
```
git clone https://gitlab.com/sentaipadas/tictactoe-frontend.git
```

2. To build the project run:
```
docker-compose build
```


## Launch 

To start the live server run:
```
docker-compose up
```

The app will be accessible on `http://localhost:8000`


## Testing

There are 3 example unit tests for testing `Board` and `Square` view components as well as redux action creator. There is one additional functional test for testing if the user sees updated views after game action in `App.test.js`. To run the tests do:
```
docker-compose run tictactoe sh -c "npm test"
```
