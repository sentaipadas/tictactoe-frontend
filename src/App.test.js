import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { act } from 'react-dom/test-utils'
import fetchMock from 'fetch-mock'


import App from './App'

import store from './store'
import { API_ROOT } from './constants'

let container

const TEST_LOG = {
  id: 1,
  player1_history: 0,
  player2_history: 0,
  turn: 0,
  move: 0,
  created: '2019-9-11',
  action: 'GAME_TOUCH'
}

const TEST_GAME = {
  id: 1,
  player1_history: 0,
  player2_history: 0,
  turn: 0,
  latest_log: TEST_LOG
}

beforeEach(() => {
  fetchMock.getOnce(`${API_ROOT}games/touch`, {
    body: TEST_GAME,
    headers: { 'content-type': 'application/json' }
  })

  fetchMock.postOnce(`${API_ROOT}games/move`, {
    body: {
      ...TEST_GAME,
      turn: 1,
      player1_history: 1,
      latest_log: {
        ...TEST_LOG,
        action: 'GAME_MOVE',
        player1_history: 1,
        move: 1,
        turn: 1,
        created: '2019-09-12'
      }
    },
    headers: { 'content-type': 'application/json' }
  })

  fetchMock.getOnce(`${API_ROOT}logs?limit=100`, {
    body: [],
    headers: { 'content-type': 'application/json' }
  })


  container = document.createElement('div')
  document.body.appendChild(container)

})

afterEach(() => {
  fetchMock.restore()
  document.body.removeChild(container)
  container = null
})

describe('Play game', () => {
  test('Updates board, logs, status after first move', async () => {
    await act(async () => {
      ReactDOM.render((
        <Provider store={store}>
          <App />
        </Provider>
      ), container)
    })
    let logs = container.querySelectorAll('.logs tr')
    expect(logs.length).toBe(2)

    const square = container.querySelector('.square')
    expect(square.textContent).toBe(' ')

    const gameStatus = container.querySelector('.status_container h3')
    expect(gameStatus.textContent).toBe('Player 1, make a move!')
    await act(async () => {
      square.dispatchEvent(new MouseEvent("click", { bubbles: true }));
    });
    expect(square.textContent).toBe('X')

    logs = container.querySelectorAll('.logs tr')
    expect(logs.length).toBe(3)

    expect(gameStatus.textContent).toBe('Player 2, make a move!')

  })
})