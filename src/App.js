import React, { useEffect } from 'react'
import { connect } from 'react-redux'

import Game from './components/game'
import Logs from './components/logs'

import { touchGame, getLogs } from './actions'

const App = ({ touchGame, getLogs }) => {

  useEffect(() => {
    getLogs().then(() => touchGame({ reset: false }))
  }, [])

  return (
    <div style={{ textAlign: 'center' }}>
      <h1>Tic Tac Toe</h1>
      <Game />
      <Logs />
    </div>
  )
}

export default connect(null, { touchGame, getLogs })(App)
