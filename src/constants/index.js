export const ACTIONS = {
  SET_GAME: 'SET_GAME',
  SET_LOGS: 'SET_LOGS',
}

export const API_ROOT = 'http://localhost:3000/api/'

export const LOG_LIMIT = 100