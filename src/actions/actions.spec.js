import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import fetchMock from 'fetch-mock'
import expect from 'expect' // You can use any testing library

import { ACTIONS, API_ROOT } from '../constants'
import * as actions from './index'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('async actions', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates SET_GAME when touch game has been done', () => {
    fetchMock.getOnce(`${API_ROOT}games/touch`, {
      body: {
        id: 1,
        player1_history: 0,
        player2_history: 0,
        turn: 0,
        latest_log: {}
      },
      headers: { 'content-type': 'application/json' }
    })

    const expectedActions = [
      {
        type: ACTIONS.SET_GAME, payload: {
          id: 1,
          player1_history: 0,
          player2_history: 0,
          turn: 0,
          latest_log: {}
        }
      }
    ]

    const store = mockStore({ game: {}, logs: [] })

    return store.dispatch(actions.touchGame({ new: false })).then(() => {
      // return of async actions
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})