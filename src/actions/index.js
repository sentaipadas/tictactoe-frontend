import { ACTIONS, API_ROOT, LOG_LIMIT } from '../constants'

export const touchGame = ({ reset }) => dispatch => handleResponse(ACTIONS.SET_GAME)(dispatch)(`${API_ROOT}games/touch${reset ? '?new=true' : ''}`)

export const makeMove = ({ bitMask }) => (dispatch, getState) => handleResponse(ACTIONS.SET_GAME)(dispatch)(`${API_ROOT}games/move`, {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    move: bitMask,
    turn: getState().game.turn + 1
  })
})

export const getLogs = () => dispatch => handleResponse(ACTIONS.SET_LOGS)(dispatch)(`${API_ROOT}logs?limit=${LOG_LIMIT}`)

const handleResponse = action => dispatch => async (url, params) => {
  try {
    const res = await fetch(url, params)
    const data = await res.json()
    if (res.status === 200) {
      dispatch({ type: action, payload: data })
    } else {
      window.alert(data[0])
    }
  } catch (e) {
    console.log(e)
    window.alert('network error ocurred')
  }
}

