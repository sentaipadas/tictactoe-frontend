import { ACTIONS } from '../constants'

const INITIAL_GAME = {
  player1_history: 0,
  player2_history: 0,
  turn: 0,
  winner: 0
}

const INITIAL_STATE = {
  game: INITIAL_GAME,
  logs: []
}

function rootReducer(state = INITIAL_STATE, action) {
  if (action.type === ACTIONS.SET_GAME) {
    const { latest_log, ...game } = action.payload
    return { ...state, game, logs: [latest_log, ...state.logs] }
  }
  if (action.type === ACTIONS.SET_LOGS) {
    return { ...state, logs: action.payload }
  }
  return state
}

export default rootReducer