import { connect } from 'react-redux'

import LogsView from './logs-view'

const mapStateToProps = state => {
  const { logs } = state
  return { logs }
}

export default connect(mapStateToProps)(LogsView)