import React from 'react'

import './logs.css'

import { BoardView } from '../board'

const COLUMN_NAMES = [
  'created',
  'action',
  'turn',
  'move',
  'winner',
]

const LogsView = ({ logs }) => (
  <div >
    <h2>Activity Log</h2>
    <table className='logs'>
      <tbody>
        <tr>
          <th>Game</th>
          {COLUMN_NAMES.map(name => <th key={name}>{name}</th>)}
        </tr>
        {logs.map(log => (
          <tr key={log.created}>
            <td className='board_display_container'><BoardView player1_history={log.player1_history} player2_history={log.player2_history} display /></td>
            {COLUMN_NAMES.map(name => <td key={name}>{log[name]}</td>)}
          </tr>
        ))}
      </tbody>
    </table>
  </div>
)

export default LogsView