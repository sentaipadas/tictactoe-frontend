import { connect } from 'react-redux'

import BoardView from './board-view'

const mapStateToProps = state => {
  const { winner, turn, player1_history, player2_history } = state.game
  return { winner, turn, player1_history, player2_history }
}

export default connect(mapStateToProps)(BoardView)