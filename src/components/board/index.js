import Board from './board-redux'
import BoardView from './board-view'

export default Board
export { BoardView }