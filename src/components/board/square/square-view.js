import React from 'react'

import './square.css'

const Square = ({ bitMask, player1_history, player2_history, makeMove = () => { } }) => {
  let value = ' '
  let empty = false

  if (player1_history & bitMask) value = 'X'
  else if (player2_history & bitMask) value = 'O'
  else empty = true

  const handleClick = () => empty && makeMove({ bitMask })

  return <div className={`square ${empty ? ' empty' : ''}`} onClick={handleClick}>{value}</div>
}

export default Square
