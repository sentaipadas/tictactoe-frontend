import React from 'react'
import ReactDOM from 'react-dom'
import { act } from 'react-dom/test-utils'

import Square from './square-view'

let container

beforeEach(() => {
  container = document.createElement('div')
  document.body.appendChild(container)
})

afterEach(() => {
  document.body.removeChild(container)
  container = null
})

describe('Display square component', () => {
  test('Displays content correctly', () => {
    act(() => {
      ReactDOM.render(<Square bitMask={1 << 0} player1_history={1} player2_history={0} />, container)
    })
    const square = container.querySelector('.square')
    expect(square.textContent).toBe('X')
  })
})