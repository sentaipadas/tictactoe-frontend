import React from 'react'
import ReactDOM from 'react-dom'
import { act } from 'react-dom/test-utils'

import Board from './board-view'

let container

beforeEach(() => {
  container = document.createElement('div')
  document.body.appendChild(container)
})

afterEach(() => {
  document.body.removeChild(container)
  container = null
})

describe('Display board component', () => {
  test('Displays content correctly', () => {
    act(() => {
      ReactDOM.render(<Board />, container)
    })
    const board = container.querySelector('.board')
    const columns = Array.from(board.children)

    expect(columns.length).toBe(3)
    expect(columns.map(col => col.className)).toEqual(Array(3).fill('column'))
  })
})