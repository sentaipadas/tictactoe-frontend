import React from 'react'

import './board.css'

import Square from './square'

const BoardView = ({ winner = 0, player1_history = 0, player2_history = 0, makeMove = () => { }, display = false }) => {
  let className = 'board'
  if (display) className += ' finished small'
  else className += winner ? ' finished' : ''

  return (
    <div className={className}>
      {Array(3).fill().map((_, i) => (
        <div className='column' key={i}>
          {Array(3).fill().map((_, j) => (
            <Square bitMask={1 << 3 * i + j} player1_history={player1_history} player2_history={player2_history} makeMove={makeMove} key={j} />
          ))}
        </div>
      ))}
    </div>
  )
}

export default BoardView