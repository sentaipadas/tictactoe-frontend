import React from 'react'

const GameStatusView = ({ turn, winner, touchGame }) => {
  let text
  if (winner) {
    if (winner === 3) text = 'draw!'
    else text = `Player ${2 - turn % 2} has won`
  } else {
    text = `Player ${2 - (turn + 1) % 2}, make a move!`
  }
  return (
    <div className='status_container'>
      <h3>{text}</h3>
      <button onClick={() => touchGame({ reset: true })}>reset game</button>
    </div>
  )
}

export default GameStatusView
