import { connect } from 'react-redux'

import GameView from './game-view'

import { makeMove, touchGame } from '../../actions'

const mapStateToProps = state => {
  const { winner, turn, player1_history, player2_history } = state.game
  return { winner, turn, player1_history, player2_history }
}

export default connect(mapStateToProps, { makeMove, touchGame })(GameView)