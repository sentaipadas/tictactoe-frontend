import React from 'react'

import './game.css'

import GameStatus from './game-status'
import Board from '../board'

const GameView = ({ winner, turn, player1_history, player2_history, makeMove, touchGame }) => (
  <div className='game_container'>
    <Board winner={winner} player1_history={player1_history} player2_history={player2_history} makeMove={makeMove} />
    <GameStatus winner={winner} turn={turn} touchGame={touchGame} />
  </div>
)

export default GameView